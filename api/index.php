<?php
	// Application name 	Angular PHP API Test
	// API key 	9d5ab90f4ec75bcd89d2f26860d1a7ea
	// Shared secret 	66c8526662b553807183c3fb26f4d20e
	// Registered to 	jchu_api
	require "services/api.php";
	require "services/geo.php";
	require "services/artist.php";

	try {
		$method = explode('?',explode("/",$_SERVER['REQUEST_URI'])[2])[0];

		if(!$method){
			throw new Exception('Invalid request. No such service.');
		}else{
			function callAPI($method){
				switch($method){
					case 'country':
						if(!$_GET['country']){
							throw new Exception('Invalid request. Supply a country parameter.');
						}else{
							$params = array(
								'country' => $_GET['country'],
								'page' => ($_GET['page'] ? $_GET['page'] : 1),
								'limit' => ($_GET['limit'] ? $_GET['limit'] : 5)
							);

							$api = new apiGeoService;
							return $api->getTopArtists($params);
						}
					case 'artist':
						if(!$_GET['artist']){
							throw new Exception('Invalid request. Supply an artist parameter.');
						}else{
							$params = array(
								'mbid' => $_GET['artist']
							);
							$api = new apiArtistService;
							return $api->getTopTracks($params);
						}
					default:
						throw new Exception("Invalid method. No such API call.");
				}
			}
			echo callAPI($method);			
		}
	}catch(Exception $e) {
        http_response_code(500);
        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => 500,
            'statusText' => 'Internal Server Error',
            'description' => $e->getMessage(),
        ));
	}
?>