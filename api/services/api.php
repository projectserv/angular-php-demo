<?php
	//generic abstract apiService class
	abstract class apiService{
		protected $root_url = "http://ws.audioscrobbler.com/2.0/";
		protected $api_key = "9d5ab90f4ec75bcd89d2f26860d1a7ea";

		protected function get($method, $params){
			$url = $this->root_url . "?api_key=" . $this->api_key . "&method=" . $method . '&' . http_build_query($params);

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$return = curl_exec($ch);
			curl_close($ch);

			$xml = simplexml_load_string($return);
			$json = json_encode($xml);

			return $json;
		}
	}
?>