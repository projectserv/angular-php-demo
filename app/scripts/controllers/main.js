'use strict';

/**
 * @ngdoc function
 * @name angularphpApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularphpApp
 */
angular.module('angularphpApp')

.controller('MainCtrl', function ($scope, $http) {

	//Set default for initial search
	$scope.country = 'Australia';
	$scope.pgMax = 5;

	//Hit API to retrieve list of artists by country
	$scope.populateArtists = function(country, limit, page){

		var params={
			'country': country,
			'limit' : (limit ? limit : 5),
			'page' : (page ? page : 1)
		};

		$http({
			method: 'GET',
			url: 'api/country',
			params: params
		}).then(function success(response){
			console.log('HTTP GET success', response);
			$scope.artists = response.data.topartists.artist;
			$scope.pgCur = response.data.topartists['@attributes'].page;
			$scope.pgCount = response.data.topartists['@attributes'].totalPages;
		}, function error(response){
			console.log('HTTP GET error', response);
		});
	};

	//Hit API with user-chosen country
	$scope.submitForm = function(){
		if($scope.mform.$valid){
			//API only returns 50 items
			if($scope.pgMax > 50){
				$scope.pgMax = 50;
			}
			if($scope.pgMax < 3){
				$scope.pgMax = 3;
			}
			$scope.populateArtists($scope.country, $scope.pgMax, 1);
		}
	};

	//Run a default search upon controller initialization
	$scope.populateArtists($scope.country);

	//Previous page
	$scope.pgPrev = function(){
		if($scope.pgCur > 0 && $scope.mform.$valid){
			$scope.pgCur--;
			$scope.populateArtists($scope.country, $scope.pgMax, $scope.pgCur);
		}
	};

	//Next page
	$scope.pgNext = function(){
		if($scope.pgCur < $scope.pgCount && $scope.mform.$valid){
			$scope.pgCur++;
			$scope.populateArtists($scope.country, $scope.pgMax, $scope.pgCur);
		}
	};
})

;//end module