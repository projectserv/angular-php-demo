'use strict';

/**
 * @ngdoc function
 * @name angularphpApp.controller:ArtistCtrl
 * @description
 * # ArtistCtrl
 * Controller of the angularphpApp
 */
angular.module('angularphpApp')

.controller('ArtistCtrl', function ($scope, $http, $routeParams) {
	$scope.params = $routeParams;

	//Hit API to retrieve artist's top tracks
	$scope.populateTracks = function(artist){

		var params={
			'artist': artist
		};

		$http({
			method: 'GET',
			url: 'api/artist',
			params: params
		}).then(function success(response){
			console.log('HTTP GET success', response);
			$scope.artist = response.data.toptracks['@attributes'].artist;
			$scope.tracks = response.data.toptracks.track;
		}, function error(response){
			console.log('HTTP GET error', response);
			$scope.invalid = true;
		});
	};

	if($routeParams.id){
		$scope.populateTracks($routeParams.id);
	}else{
		$scope.invalid = true;
	}
})

;
