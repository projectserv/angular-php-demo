'use strict';

/**
 * @ngdoc function
 * @name angularphpApp.controller:NavCtrl
 * @description
 * # NavCtrl
 * Controller of the angularphpApp
 */
angular.module('angularphpApp')

.controller('NavCtrl', function ($scope, $location) {
	$scope.active = function(location){
		return location === $location.path();
	};
})

;
