'use strict';

/**
 * @ngdoc function
 * @name angularphpApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularphpApp
 */
angular.module('angularphpApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
