'use strict';

/**
 * @ngdoc directive
 * @name angularphpApp.directive:apArtist
 * @description
 * # apArtist
 */
angular.module('angularphpApp')
  .directive('apArtist', function () {
    return {
      templateUrl: 'views/ap-artist.html',
      restrict: 'E'
      //link: function postLink(scope, element, attrs) {}
    };
  });
