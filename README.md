# AngularJS / PHP Demo Application

This project exists to demonstrate an AngularJS front end accessing a PHP API.

## Build & development

- Yeoman used to scaffold out the application.
- Bootstrap included to get the design side ready as quickly as possible.
- SASS/Compass included as standard.
- Grunt used for automated watch/live reload, linting and deployment.
- grunt-connect-proxy used to hit API.

## Project dependencies

- node
- npm
- bower
- grunt

## Running the project

Clone the project.

```git clone https://gitlab.com/projectserv/angular-php-demo.git```

Pull in project dependencies.

```npm install```

```bower install```

Open a new terminal window and navigate to /api, then start the API server with its router file.

```php -S localhost: 8080 router.php```

In the original terminal window, start the app.

```grunt serve```

## Live instance

http://35.162.26.161:9000/#/ on Amazon EC2